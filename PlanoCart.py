import pygame
import Geometria

background_colour = (255,255,255) # White color
(width, height) = (800, 600) # Screen size
color=(0,0,0) #For retangle
color1=(149,16,45)
color2= (255,177,9)
color3= (255,0,0)
color4= (90,118,225)
color5= (14,205,21)
screen = pygame.display.set_mode((width, height)) #Setting Screen
pygame.display.set_caption('Plano cartesiano') #Window Name
screen.fill(background_colour)#Fills white to screen
#Drawing the Plano Cartesiano

pygame.draw.line(screen,color1, (400,0), (400,600),3)
pygame.draw.line(screen,color1, (0,300), (800,300),3)
pygame.draw.circle(screen,color3, (400,300),(5),0)

#Drawing Lines to Plano cartesiano Eje Y
pygame.draw.line(screen,color2, (50,0), (50,600),1)
pygame.draw.line(screen,color2, (100,0), (100,600),1)
pygame.draw.line(screen,color2, (150,0), (150,600),1)
pygame.draw.line(screen,color2, (200,0), (200,600),1)
pygame.draw.line(screen,color2, (250,0), (250,600),1)
pygame.draw.line(screen,color2, (300,0), (300,600),1)
pygame.draw.line(screen,color2, (350,0), (350,600),1)
pygame.draw.line(screen,color2, (450,0), (450,600),1)
pygame.draw.line(screen,color2, (500,0), (500,600),1)
pygame.draw.line(screen,color2, (550,0), (550,600),1)
pygame.draw.line(screen,color2, (600,0), (600,600),1)
pygame.draw.line(screen,color2, (650,0), (650,600),1)
pygame.draw.line(screen,color2, (700,0), (700,600),1)
pygame.draw.line(screen,color2, (750,0), (750,600),1)
#Drawing Lines to Plano cartesiano Eje x
pygame.draw.line(screen,color3, (0,50), (800,50),1)
pygame.draw.line(screen,color3, (0,100), (800,100),1)
pygame.draw.line(screen,color3, (0,150), (800,150),1)
pygame.draw.line(screen,color1, (0,200), (800,200),1)
pygame.draw.line(screen,color1, (0,250), (800,250),1)
pygame.draw.line(screen,color1, (0,350), (800,350),1)
pygame.draw.line(screen,color1, (0,400), (800,400),1)
pygame.draw.line(screen,color1, (0,450), (800,450),1)
pygame.draw.line(screen,color1, (0,500), (800,500),1)
pygame.draw.line(screen,color1, (0,550), (800,550),1)
pygame.draw.line(screen,color1, (0,600), (800,600),1)
pygame.draw.line(screen,color1, (0,650), (800,650),1)
pygame.draw.line(screen,color1, (0,700), (800,700),1)
pygame.draw.line(screen,color1, (0,750), (800,750),1)


#Eje x

pygame.draw.line(screen,color2, (50,290), (50,310),3)
pygame.draw.line(screen,color2, (100,290), (100,310),3)
pygame.draw.line(screen,color2, (150,290), (150,310),3)
pygame.draw.line(screen,color2, (200,290), (200,310),3)
pygame.draw.line(screen,color2, (250,290), (250,310),3)
pygame.draw.line(screen,color2, (300,290), (300,310),3)
pygame.draw.line(screen,color2, (350,290), (350,310),3)
pygame.draw.line(screen,color2, (450,290), (450,310),3)
pygame.draw.line(screen,color2, (500,290), (500,310),3)
pygame.draw.line(screen,color2, (550,290), (550,310),3)
pygame.draw.line(screen,color2, (600,290), (600,310),3)
pygame.draw.line(screen,color2, (650,290), (650,310),3)
pygame.draw.line(screen,color2, (700,290), (700,310),3)
pygame.draw.line(screen,color2, (750,290), (750,310),3)
#Eje y
pygame.draw.line(screen,color2, (390,50), (410,50),3)
pygame.draw.line(screen,color2, (390,100), (410,100),3)
pygame.draw.line(screen,color2, (390,150), (410,150),3)
pygame.draw.line(screen,color2, (390,200), (410,200),3)
pygame.draw.line(screen,color2, (390,250), (410,250),3)
pygame.draw.line(screen,color2, (390,350), (410,350),3)
pygame.draw.line(screen,color2, (390,400), (410,400),3)
pygame.draw.line(screen,color2, (390,450), (410,450),3)
pygame.draw.line(screen,color2, (390,500), (410,500),3)
pygame.draw.line(screen,color2, (390,550), (410,550),3)

#Drawing the Rectangle from geometria.

pygame.draw.rect(screen,color5,(400,300, 150, -100))

#Drawing Punto A
pygame.draw.circle(screen,color4, (500,150),(5),0)
#Drawing Punto B
pygame.draw.circle(screen,color4, (650,50),(5),0)
#Drawing punto C
pygame.draw.circle(screen,color4, (250,350),(5),0)
#Drawing Punto D
pygame.draw.circle(screen,color4, (400,300),(5),0)



pygame.display.update()


running = True
while running:
  for event in pygame.event.get():
    if event.type == pygame.QUIT:
      running = False
      pygame.quit()
